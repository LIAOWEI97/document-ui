import Vue from 'vue'
import VueRouter from 'vue-router'

const Login = () => import('../components/Login')
const Home = () => import('../views/Home')
const HelloWorld = () => import('../components/HelloWorld')

Vue.use(VueRouter)


const routes =  [
  {
    path: '/login',
    component: Login,
    meta: {
      title: '登录'
    }
  },
  {
    path: '/home',
    component: Home,
    meta: {
      title: '主页'
    },
    children: [
      {
        path: '/helloWorld',
        component: HelloWorld,
        meta: {
          title: 'helloWorld'
        }
      }
    ]
  },
  {
    path: '/',
    redirect: '/login'
  }
]

const router = new VueRouter({
  routes: routes,
  mode: 'history',
  linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})


export default router
